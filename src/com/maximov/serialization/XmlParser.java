package com.maximov.serialization;

import org.w3c.dom.*;

import java.lang.reflect.*;
import java.util.*;

public class XmlParser<T> {
    private final Document doc;
    private final T item;
    private final Class<T> itemClass;
    private final Element objectElement;

    public final static Map<String, Class<?>> primitiveWrappersMap = new HashMap<>();
    static {
        primitiveWrappersMap.put("boolean", Boolean.class);
        primitiveWrappersMap.put("byte", Byte.class);
        primitiveWrappersMap.put("short", Short.class);
        primitiveWrappersMap.put("char", Character.class);
        primitiveWrappersMap.put("int", Integer.class);
        primitiveWrappersMap.put("long", Long.class);
        primitiveWrappersMap.put("float", Float.class);
        primitiveWrappersMap.put("double", Double.class);
    }


    public XmlParser(Document doc, ClassLoader classLoader) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        this.doc = doc;
        this.objectElement =  this.doc.getDocumentElement();
        String className = objectElement.getAttribute("type");
        this.itemClass = (Class<T>) (classLoader == null ? Class.forName(className) : classLoader.loadClass(className));
        this.item = this.itemClass.newInstance();
    }

    public T parse() throws NoSuchFieldException, IllegalAccessException,
            ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException {
        this.setItemFields();
        return this.item;
    }

    private void setItemFields() throws NoSuchFieldException, IllegalAccessException,
            ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException {
        NodeList fieldsdList = objectElement.getElementsByTagName("field");
        for(int i = 0; i < fieldsdList.getLength();i++){
            setItemField((Element) fieldsdList.item(i));
        }
    }

    private void setItemField(Element fieldElement) throws NoSuchFieldException, IllegalAccessException,
            ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException {
        Field field = this.itemClass.getDeclaredField(fieldElement.getAttribute("id"));
        field.setAccessible(true);

        Class fieldClass = getClassByName(fieldElement.getAttribute("type"));
        String fieldValue = fieldElement.getAttribute("value");
        field.set(this.item, fieldClass.getConstructor(String.class).newInstance(fieldValue));
    }

    private Class getClassByName(String className) throws ClassNotFoundException {
        return primitiveWrappersMap.containsKey(className) ? primitiveWrappersMap.get(className) : Class.forName(className);
    }
}
