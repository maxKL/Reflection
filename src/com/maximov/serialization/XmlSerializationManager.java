package com.maximov.serialization;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * Сериализатор сущностей в xml на основе рефлексии
 */
public class XmlSerializationManager {
    private DocumentBuilder builder;
    private DOMImplementation impl;
    private Transformer transformer;

    public XmlSerializationManager() throws ParserConfigurationException, TransformerConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        this.builder = factory.newDocumentBuilder();
        this.impl = builder.getDOMImplementation();

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        this.transformer = transformerFactory.newTransformer();
    }

    /**
     *
     * @param item
     * @param <T>
     * @throws IllegalAccessException
     * @throws TransformerException
     * @apiNote Производит сериализацию
     */
    public <T> String serialize(T item) throws IllegalAccessException, TransformerException {
        Document document = getNewDocument();
        XmlFormer<T> xmlFormer = new XmlFormer<>(document, item);
        document = xmlFormer.form();
        return saveDocument(document, item.toString());
    }

    private Document getNewDocument() {
        return impl.createDocument(null,null, null);
    }

    private String saveDocument(Document doc, String documentName) throws TransformerException {
        DOMSource source = new DOMSource(doc);
        String documentPath = documentName + ".xml";
        StreamResult result = new StreamResult(new File(documentPath));

        this.transformer.transform(source, result);

        return documentPath;
    }

    public <T> T deserialize(String filePath, ClassLoader classLoader) throws IOException, SAXException, ClassNotFoundException,
            IllegalAccessException, InstantiationException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        Document doc = getExcitingDocument(filePath);
        XmlParser<T> xmlParser = new XmlParser<T>(doc, classLoader);
        return xmlParser.parse();
    }

    private Document getExcitingDocument(String filePath) throws IOException, SAXException {
        File fXmlFile = new File(filePath);
        return this.builder.parse(fXmlFile);
    }
}
