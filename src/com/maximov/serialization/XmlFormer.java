package com.maximov.serialization;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.lang.reflect.Field;

class XmlFormer<T> {
    private final Document doc;
    private final T item;
    private final Class itemClass;
    private final Element objectElement;

    public XmlFormer(Document doc, T item){
        this.doc = doc;
        this.item = item;
        this.itemClass = item.getClass();
        this.objectElement = addObjectElement();
    }

    private Element addObjectElement() {
        Element objectElement = this.doc.createElement("object");
        objectElement.setAttribute("type", itemClass.getName());
        doc.appendChild(objectElement);

        return objectElement;
    }

    public Document form() throws IllegalAccessException {
        addFieldElements();
        return this.doc;
    }

    private void addFieldElements() throws IllegalAccessException {
        for(Field itemField : itemClass.getDeclaredFields()){
            itemField.setAccessible(true);
            addFieldElement(itemField);
        }
    }

    private void addFieldElement(Field itemField) throws IllegalAccessException {
        Element fieldElement = doc.createElement("field");
        fieldElement.setAttribute("type", itemField.getType().getTypeName());
        fieldElement.setAttribute("id", itemField.getName());
        fieldElement.setAttribute("value", itemField.get(item).toString());

        objectElement.appendChild(fieldElement);
    }


}
