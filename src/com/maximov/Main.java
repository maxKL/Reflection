package com.maximov;

import com.maximov.classloader.GitClassLoader;
import com.maximov.serialization.XmlSerializationManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class Main {

    public static void main(String[] args) {
        try {
            GitClassLoader loader = new GitClassLoader("https://gitlab.com/maxKL/ProjectWithPeopleClass/raw/master/src/ProjectWithPeopleClass_jar.jar");
            Class<?> clazz =
                    loader.loadClass("People");
            Constructor peopleConstructor = clazz.getConstructor(String.class, Integer.class, double.class);

            Object[] people = {
                peopleConstructor.newInstance("John Dow", 30, 23000),
                peopleConstructor.newInstance("Dr Who", 1000, 371000),
                peopleConstructor.newInstance("Goblin", 44, 50000)
            };

            XmlSerializationManager serializationManager = new XmlSerializationManager();

            String[] filePathes = new String[people.length];
            for(int i = 0; i < people.length;i++){
                filePathes[i] = serializationManager.serialize(people[i]);
            }

            System.out.println("Десериализованные объекты:");
            Field nameField = clazz.getDeclaredField("name");
            nameField.setAccessible(true);
            Field ageField = clazz.getDeclaredField("age");
            ageField.setAccessible(true);
            Field salaryField = clazz.getDeclaredField("salary");
            salaryField.setAccessible(true);

            for(int i = 0; i < people.length;i++){
                Object deserializedPeople = serializationManager.deserialize(filePathes[i], loader);
                System.out.println(nameField.get(deserializedPeople) + " "
                        + ageField.get(deserializedPeople) + " "
                        + salaryField.get(deserializedPeople));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
